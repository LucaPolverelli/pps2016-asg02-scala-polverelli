package utils;

/**
 * @author Roberto Casadei
 */

public class Res {
    public static final String IMG_BASE = "/resources/imgs/";
    public static final String AUDIO_BASE = "/resources/audio/";
    public static final String IMG_EXT = ".png";

    public static final String IMGP_STATUS_NORMAL = "";
    public static final String IMGP_STATUS_ACTIVE = "A";
    public static final String IMGP_STATUS_DEAD = "E";
    public static final String IMGP_STATUS_IDLE = "F";
    public static final String IMGP_STATUS_SUPER = "S";

    public static final String IMGP_DIRECTION_SX = "G";
    public static final String IMGP_DIRECTION_DX = "D";

    public static final String MUSHROOM = "mushroom";
    public static final String TURTLE = "turtle";
    public static final String MARIO = "mario";

    public static final String BLOCK = "block";
    public static final String PIECE = "piece";
    public static final String PIECE1 = "piece1";
    public static final String PIECE2 = "piece2";
    public static final String TUNNEL = "tunnel";
    public static final String STAR = "star";
    public static final String ONEUP = "oneup";

    public static final String IMG_MARIO_SUPER_SX = IMG_BASE + MARIO + IMGP_STATUS_SUPER + IMGP_DIRECTION_SX + IMG_EXT;
    public static final String IMG_MARIO_SUPER_DX = IMG_BASE + MARIO + IMGP_STATUS_SUPER + IMGP_DIRECTION_DX + IMG_EXT;
    public static final String IMG_MARIO_ACTIVE_SX = IMG_BASE + MARIO + IMGP_STATUS_ACTIVE + IMGP_DIRECTION_DX + IMG_EXT;
    public static final String IMG_MARIO_ACTIVE_DX = IMG_BASE + MARIO + IMGP_STATUS_ACTIVE + IMGP_DIRECTION_DX + IMG_EXT;

    public static final String IMG_MUSHROOM_DEAD_DX = IMG_BASE + MUSHROOM + IMGP_STATUS_DEAD + IMGP_DIRECTION_DX + IMG_EXT;
    public static final String IMG_MUSHROOM_DEAD_SX = IMG_BASE + MUSHROOM + IMGP_STATUS_DEAD + IMGP_DIRECTION_SX + IMG_EXT;

    public static final String IMG_TURTLE_IDLE = IMG_BASE + TURTLE + IMGP_STATUS_IDLE + IMG_EXT;
    public static final String IMG_TURTLE_DEAD = IMG_TURTLE_IDLE;

    public static final String IMG_BLOCK = IMG_BASE + BLOCK + IMG_EXT;

    public static final String IMG_STAR = IMG_BASE + STAR + IMG_EXT;

    public static final String IMG_ONEUP = IMG_BASE + ONEUP + IMG_EXT;

    public static final String IMG_PIECE1 = IMG_BASE + PIECE1 + IMG_EXT;
    public static final String IMG_PIECE2 = IMG_BASE + PIECE2 + IMG_EXT;
    public static final String IMG_TUNNEL = IMG_BASE + TUNNEL + IMG_EXT;

    public static final String IMG_BACKGROUND = IMG_BASE + "background" + IMG_EXT;
    public static final String IMG_CASTLE = IMG_BASE + "castelloIni" + IMG_EXT;
    public static final String START_ICON = IMG_BASE + "start" + IMG_EXT;
    public static final String IMG_CASTLE_FINAL = IMG_BASE + "castelloF" + IMG_EXT;
    public static final String IMG_FLAG = IMG_BASE + "bandiera" + IMG_EXT;

    public static final String AUDIO_MONEY = AUDIO_BASE + "money.wav";
}
