package entities

/**
  * Created by Luca on 12/03/2017.
  */
trait Entity {

  def width: Int
  def height: Int
  def x: Int
  def x_=(x: Int): Unit
  def y: Int
  def y_=(y: Int): Unit
  def moveOnSceneShift(): Unit
}

class BasicEntity(override var x: Int, override var y: Int, override val width: Int, override val height: Int) extends Entity {
  import game.Main
  override def moveOnSceneShift() = if (Main.scene.getXPos >= 0) x = x - Main.scene.getMov
}

object Entity {
  def apply(x: Int, y: Int, width: Int, height: Int): Entity = new BasicEntity(x, y, width, height)
}