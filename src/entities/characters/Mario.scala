package entities.characters

import java.awt.Image

import entities.Entity
import entities.objects.Collectible
import entities.objects.Collectible.{OneUp, Star}
import entities.objects.Object
import game.Main
import utils.Res
import utils.Utils

trait Mario extends Character{
  def jumping: Boolean
  def jumping_=(jumping: Boolean): Unit
  def doJump(): Image
  def contact(entity: Entity): Unit
  def contactCollectible(collectible: Collectible): Boolean
  def power: Boolean
  def power_=(power: Boolean): Unit
  def walk(name: String): Image
}

object Mario{
  def apply(x: Int, y: Int): Mario = new MarioImpl(x, y)

  private val MARIO_FREQUENCY = 25
  private val MARIO_OFFSET_Y_INITIAL = 243
  private val FLOOR_OFFSET_Y_INITIAL = 293
  private val WIDTH = 28
  private val HEIGHT = 50
  private val DEFAULT_JUMP_LIMIT = 42
  private val POWER_TIME = 10000
  private val INVINCIBILITY_FRAMES_TIME = 1000

  class MarioImpl(startingX: Int, startingY: Int) extends BasicCharacter(startingX, startingY, WIDTH, HEIGHT) with Mario {
    private var jumpingExtent = 0
    private var jumpLimit = DEFAULT_JUMP_LIMIT
    private var lives = 2
    private var invincibilityFrames = false

    override var jumping = false
    override var power = false

    override def doJump(): Image = {
      var string = new String
      this.jumpingExtent += 1
      if (this.jumpingExtent < jumpLimit) {
        if (this.y > Main.scene.getHeightLimit) this.y = this.y - 4 else this.jumpingExtent = this.jumpLimit
        string = if (this.toRight) Res.IMG_MARIO_SUPER_DX else Res.IMG_MARIO_SUPER_SX
      }
      else if (this.y + this.height < Main.scene.getFloorOffsetY) {
        this.y = this.y + 1
        string = if (this.toRight) Res.IMG_MARIO_SUPER_DX else Res.IMG_MARIO_SUPER_SX
      }
      else {
        string = if (this.toRight) Res.IMG_MARIO_ACTIVE_DX else Res.IMG_MARIO_ACTIVE_SX
        this.jumping = false
        this.jumpingExtent = 0
      }
      Utils.getImage(string)
    }

    override def contact(entity: Entity) = entity match{
      case entity:Character => contactCharacter(entity);
      case entity:Object => contactObject(entity);
    }

    private def contactObject(`object`: Object) = {
      if ((this hitAhead `object`) && this.toRight || (this hitBack `object`) && !this.toRight) {
        Main.scene.setMov(0)
        this.moving = false
      }
      if ((this hitBelow `object`) && this.jumping) Main.scene.setFloorOffsetY(`object`.y)
      else if (!(this hitBelow `object`)) {
        Main.scene.setFloorOffsetY(FLOOR_OFFSET_Y_INITIAL)
        if (!this.jumping) this.y = MARIO_OFFSET_Y_INITIAL
        if (this hitAbove `object`) Main.scene.setHeightLimit(`object`.y + `object`.height)
        else if (!(this hitAbove `object`) && !this.jumping) Main.scene.setHeightLimit(0)
      }
    }

    private def contactCharacter(character: Character) = {
      def killCharacter(character: Character) = {
        character.moving = false
        character.alive = false
      }

      def die() = {
        this.moving = false
        this.alive = false
      }

      def startInvincibilityFrames() = {
        this.invincibilityFrames = true
        new Thread(() => {
          try Thread.sleep(INVINCIBILITY_FRAMES_TIME)
          catch {
            case e: InterruptedException => e.printStackTrace()
          }
          finally this.invincibilityFrames = false
        }).start()
      }

      def updateLives(): Unit ={
        this.lives -= 1
        if(Main.scene !=  null) Main.scene.updateLives(this.lives)
      }

      if (character.alive) {
        if ((this hitAhead character) || (this hitBack character)) {
          if (this.power) killCharacter(character)
          else if (!this.invincibilityFrames) this.lives match {
            case 0 => die()
            case _ => startInvincibilityFrames(); updateLives()
          }
        }
        else if (this hitBelow character) killCharacter(character)
      }
    }

    override def contactCollectible(collectible: Collectible): Boolean = {

      def startPower() = {
        this.power = true
        new Thread(() => {
          this.jumpLimit = DEFAULT_JUMP_LIMIT * 2
          try Thread.sleep(POWER_TIME)
          catch {
            case e: InterruptedException => e.printStackTrace()
          }
          finally this.power = false; this.jumpLimit = DEFAULT_JUMP_LIMIT
        }).start()
      }

      if ((this hitBack collectible) || (this hitAbove collectible) || (this hitAhead collectible) || (this hitBelow collectible)) {
        collectible match {
          case Star(_,_) => startPower()
          case OneUp(_,_) => this.lives += 1; Main.scene.updateLives(this.lives)
          case _ =>
        }
        return true
      }
      false
    }

    override def walk(name: String): Image = super.walk(name, MARIO_FREQUENCY)

  }

}

