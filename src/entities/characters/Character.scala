package entities.characters

import entities.{BasicEntity, Entity}
import java.awt.Image

import utils.{Res, Utils}

trait Character extends Entity {
  def alive: Boolean
  def alive_=(alive: Boolean): Unit
  def toRight: Boolean
  def toRight_=(toRight: Boolean): Unit
  def moving: Boolean
  def moving_=(moving: Boolean): Unit
  def walk(name: String, frequency: Int): Image
  def isEntityNearby(entity: Entity): Boolean
}

object BasicCharacter {
  private val PROXIMITY_MARGIN = 10
  private val HIT_MARGIN = 5
}

class BasicCharacter(startingX: Int, startingY: Int, width: Int, height: Int)
  extends BasicEntity(startingX, startingY, width, height) with Character {

  private var counter = 0

  private var firstEntityMarginLeft = 0
  private var firstEntityMarginRight = 0
  private var firstEntityMarginTop = 0
  private var firstEntityMarginBottom = 0

  private var secondEntityMarginLeft = 0
  private var secondEntityMarginLeftWithMargin = 0
  private var secondEntityMarginRight = 0
  private var secondEntityMarginRightWithMargin = 0
  private var secondEntityMarginTop = 0
  private var secondEntityMarginTopWithMargin = 0
  private var secondEntityMarginBottom = 0
  private var secondEntityMarginBottomWithMargin = 0

  override var moving = false
  override var toRight = true
  override var alive = true

  import Res._
  override def walk(name: String, frequency: Int): Image = {
    counter += 1
    val str = IMG_BASE + name +
      (if (!this.moving || this.counter % frequency == 0) IMGP_STATUS_ACTIVE else IMGP_STATUS_NORMAL) +
      (if (this.toRight) IMGP_DIRECTION_DX else IMGP_DIRECTION_SX) + IMG_EXT
    Utils.getImage(str)
  }

  import BasicCharacter._
  override def isEntityNearby(entity: Entity): Boolean = {
    updateMargins(entity)
    (firstEntityMarginLeft > secondEntityMarginLeft - PROXIMITY_MARGIN &&
      firstEntityMarginLeft < secondEntityMarginRight + PROXIMITY_MARGIN) ||
      (firstEntityMarginRight > secondEntityMarginLeft - PROXIMITY_MARGIN &&
        firstEntityMarginRight < secondEntityMarginRight + PROXIMITY_MARGIN)
  }

  protected def hitAhead(entity: Entity): Boolean = {
    updateMargins(entity)
    (entity, this.toRight) match {
      case (_: BasicCharacter, false) => false
      case _ => !(firstEntityMarginRight < secondEntityMarginLeft ||
        firstEntityMarginRight > secondEntityMarginLeftWithMargin ||
        firstEntityMarginTop <= secondEntityMarginBottom ||
        firstEntityMarginBottom >= secondEntityMarginTop)
    }
  }

  protected def hitBack(entity: Entity): Boolean = {
    updateMargins(entity)
    !(firstEntityMarginLeft > secondEntityMarginRight ||
      firstEntityMarginRight < secondEntityMarginRightWithMargin ||
      firstEntityMarginTop <= secondEntityMarginBottom ||
      firstEntityMarginBottom >= secondEntityMarginTop)
  }

  protected def hitBelow(entity: Entity): Boolean = {
    updateMargins(entity)
    entity match {
      case _:BasicCharacter => !(firstEntityMarginRight < secondEntityMarginLeft ||
        firstEntityMarginLeft > secondEntityMarginRight ||
        firstEntityMarginTop < secondEntityMarginBottom ||
        firstEntityMarginTop > secondEntityMarginBottom)
      case _ => !(firstEntityMarginRight < secondEntityMarginLeftWithMargin ||
        firstEntityMarginLeft > secondEntityMarginRightWithMargin ||
        firstEntityMarginTop < secondEntityMarginBottom ||
        firstEntityMarginTop > secondEntityMarginBottomWithMargin)
    }
  }

  protected def hitAbove(entity: Entity): Boolean = {
    updateMargins(entity)
    !(firstEntityMarginRight < secondEntityMarginLeftWithMargin ||
      firstEntityMarginLeft > secondEntityMarginRightWithMargin ||
      firstEntityMarginBottom < secondEntityMarginTop ||
      firstEntityMarginBottom > secondEntityMarginTopWithMargin)
  }

  private def updateMargins(entity: Entity) = {
    firstEntityMarginLeft = this.x
    firstEntityMarginRight = this.x + this.width
    firstEntityMarginTop = this.y + this.height
    firstEntityMarginBottom = this.y

    secondEntityMarginLeft = entity.x
    secondEntityMarginLeftWithMargin = entity.x + HIT_MARGIN
    secondEntityMarginRight = entity.x + entity.width
    secondEntityMarginRightWithMargin = entity.x + entity.width - HIT_MARGIN
    secondEntityMarginTop = entity.y + entity.height
    secondEntityMarginTopWithMargin = entity.y + entity.height + HIT_MARGIN
    secondEntityMarginBottom = entity.y
    secondEntityMarginBottomWithMargin = entity.y + HIT_MARGIN
  }
}