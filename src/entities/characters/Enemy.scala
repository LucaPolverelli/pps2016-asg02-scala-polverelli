package entities.characters

import entities.Entity
import java.awt._

import utils.{Res, Utils}

/**
  * Created by Luca on 12/03/2017.
  */
trait Enemy extends Character {
  def move(): Unit
  def contact(entity: Entity): Unit
  def deadImage: Image
  def walk: Image
  def deadOffsetY: Int
}

abstract class AbstractEnemy(startingX: Int, startingY: Int, width: Int, height: Int)
  extends BasicCharacter(startingX, startingY, width, height) with Enemy {

  private var offsetX = 1

  override def move(): Unit = {
    this.setOffsetX (if (this.toRight) 1 else -1)
    this.x = this.x + this.getOffsetX
  }

  override def contact(entity: Entity): Unit = {
    if (this.hitAhead(entity) && this.toRight) {
      this.toRight = false
      this.offsetX = -1
    }
    else if (this.hitBack(entity) && !this.toRight) {
      this.toRight = true
      this.offsetX = 1
    }
  }

  override def deadImage: Image = this.getDeadImage

  override def walk: Image = this.doWalk()

  override def deadOffsetY: Int = this.getDeadOffsetY

  protected def getDeadImage: Image

  protected def doWalk(): Image

  protected def getDeadOffsetY: Int

  private def setOffsetX(offsetX: Int) = this.offsetX = offsetX

  private def getOffsetX = this.offsetX
}

object Enemy{
  def apply(`type`:String, x:Int, y:Int): Enemy = `type` match {
    case Res.TURTLE => new Turtle(x, y)
    case Res.MUSHROOM => new Mushroom(x, y)
  }

  object Turtle{
    private val TURTLE_DEAD_OFFSET_Y = 30
    private val TURTLE_FREQUENCY = 45
    private val WIDTH = 43
    private val HEIGHT = 50
    private val PAUSE = 15
  }

  object Mushroom{
    private val MUSHROOM_DEAD_OFFSET_Y = 20
    private val MUSHROOM_FREQUENCY = 45
    private val WIDTH = 27
    private val HEIGHT = 30
    private val PAUSE = 15
  }

  import Turtle._
  case class Turtle(startingX: Int, startingY: Int) extends AbstractEnemy(startingX, startingY, WIDTH, HEIGHT) with Runnable {
    this.toRight = true
    this.moving = true
    val chronoTurtle = new Thread(this, Res.TURTLE)
    chronoTurtle.start()


    override def run(): Unit = {
      while (this.alive) {
        move()
        try Thread.sleep(PAUSE)
        catch {
          case e: InterruptedException => e.printStackTrace()
        }
      }
    }

    override protected def getDeadImage: Image = Utils.getImage(Res.IMG_TURTLE_DEAD)

    override protected def doWalk(): Image = walk(Res.TURTLE, TURTLE_FREQUENCY)

    override protected def getDeadOffsetY = TURTLE_DEAD_OFFSET_Y
  }

  import Mushroom._
  case class Mushroom(startingX: Int, startingY: Int) extends AbstractEnemy(startingX, startingY, WIDTH, HEIGHT) with Runnable {
    this.toRight = true
    this.moving = true
    val chronoMushroom = new Thread(this, Res.MUSHROOM)
    chronoMushroom.start()

    override def run(): Unit = {
      while (this.alive) {
        move()
        try Thread.sleep(PAUSE)
        catch {
          case e: InterruptedException => e.printStackTrace()
        }
      }
    }

    override protected def getDeadImage: Image = Utils.getImage(if (this.toRight) Res.IMG_MUSHROOM_DEAD_DX else Res.IMG_MUSHROOM_DEAD_SX)

    override protected def doWalk(): Image = walk(Res.MUSHROOM, MUSHROOM_FREQUENCY)

    override protected def getDeadOffsetY = MUSHROOM_DEAD_OFFSET_Y
  }
}