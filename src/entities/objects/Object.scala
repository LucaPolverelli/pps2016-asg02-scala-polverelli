package entities.objects

import entities.{BasicEntity, Entity}
import java.awt._

import utils.{Res, Utils}

/**
  * Created by Luca on 12/03/2017.
  */
trait Object extends Entity {
  def imageObject: Image
}

object Object {
  def apply(`type`:String, x:Int, y:Int): Object = `type` match {
    case Res.TUNNEL => new Tunnel(x, y)
    case Res.BLOCK => new Block(x, y)
  }

  class BasicObject(x: Int, y: Int, width: Int, height: Int, private val _imageObject: Image)
    extends BasicEntity(x, y, width, height) with Object {
    override def imageObject: Image = _imageObject
  }

  object Tunnel {
    private val WIDTH = 43
    private val HEIGHT = 65
    private val IMAGE = Utils.getImage(Res.IMG_TUNNEL)
  }

  object Block {
    private val WIDTH = 30
    private val HEIGHT = 30
    private val IMAGE = Utils.getImage(Res.IMG_BLOCK)
  }

  import Tunnel._
  class Tunnel(x: Int, y: Int) extends BasicObject(x, y, WIDTH, HEIGHT, IMAGE)

  import Block._
  class Block(x: Int, y: Int) extends BasicObject(x, y, WIDTH, HEIGHT, IMAGE)
}
