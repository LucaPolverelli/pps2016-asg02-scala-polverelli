package entities.objects

import java.awt.Image

import entities.objects.Object.BasicObject
import utils.{Res, Utils}

/**
  * Created by Luca on 14/03/2017.
  */
trait Collectible extends Object

object Collectible{
  def apply(`type`:String, x:Int, y:Int): Collectible = `type` match {
    case Res.PIECE => Piece(x, y)
    case Res.STAR => Star(x, y)
    case Res.ONEUP => OneUp(x, y)
  }

  object Piece{
    private val WIDTH = 30
    private val HEIGHT = 30
    private val PAUSE = 10
    private val FLIP_FREQUENCY = 100
    private val IMAGE = Utils.getImage(Res.IMG_PIECE1)
  }

  object Star{
    private val WIDTH = 30
    private val HEIGHT = 30
    private val IMAGE = Utils.getImage(Res.IMG_STAR)
  }

  object OneUp {
    private val WIDTH = 30
    private val HEIGHT = 30
    private val IMAGE = Utils.getImage(Res.IMG_ONEUP)
  }

  import Piece._
  case class Piece(startingX:Int, startingY:Int) extends BasicObject(startingX, startingY, WIDTH, HEIGHT, IMAGE) with Runnable with Collectible {

    private var counter = 0
    private val chronoPiece = new Thread(this, Res.PIECE)
    chronoPiece.start()

    override def imageObject: Image = imageOnMovement()

    override def run(): Unit = {
      while (true) {
        imageOnMovement()
        try Thread.sleep(Piece.PAUSE)
        catch {
          case e: InterruptedException => e.printStackTrace()
        }
      }
    }

    private def imageOnMovement() = {
      counter += 1
      Utils.getImage( counter % FLIP_FREQUENCY match {
        case 0 => Res.IMG_PIECE1
        case _ => Res.IMG_PIECE2
      })
    }
  }

  import Star._
  case class Star(startingX: Int, startingY: Int) extends BasicObject(startingX, startingY, WIDTH, HEIGHT, IMAGE) with Collectible

  import OneUp._
  case class OneUp(startingX: Int, startingY: Int) extends BasicObject(startingX, startingY, WIDTH, HEIGHT, IMAGE) with Collectible
}