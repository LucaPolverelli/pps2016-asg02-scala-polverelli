package game;

import javax.swing.*;

public class Main {

    private static final int WINDOW_WIDTH = 700;
    private static final int WINDOW_HEIGHT = 360;
    private static final String WINDOW_TITLE = "Super Mario";
    public static Platform scene;

    public static void main(String[] args) {
        JFrame window = new JFrame(WINDOW_TITLE);
        window.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        window.setSize(WINDOW_WIDTH, WINDOW_HEIGHT);
        window.setLocationRelativeTo(null);
        window.setResizable(true);
        window.setAlwaysOnTop(true);

        scene = new Platform();

        window.setContentPane(scene);
        window.setVisible(true);

        Thread timer = new Thread(new Refresh(), WINDOW_TITLE);
        timer.start();
    }

}
