package game

import java.awt.{Graphics, Graphics2D}
import javax.swing.JPanel

import entities.characters._
import entities.objects.Collectible.Piece
import entities.objects._
import utils.{Res, Utils}

object Platform extends JPanel{
  private val FLAG_X_POSITION = 4650
  private val CASTLE_X_POSITION = 4850
  private val FLAG_Y_POSITION = 115
  private val CASTLE_Y_POSITION = 145
  private val LIVES_X_POSITION = 20
  private val LIVES_Y_POSITION = 20
  private val LIVES_STRING = "LIVES: "
}

class Platform() extends JPanel {
  private var background1PosX = -50
  private var background2PosX = 750
  private var mov = 0
  private var xPos = -1
  private var floorOffsetY = 293
  private var heightLimit = 0
  private var lives = 2

  private val imgBackground1 = Utils.getImage(Res.IMG_BACKGROUND)
  private val imgBackground2 = Utils.getImage(Res.IMG_BACKGROUND)
  private val castle = Utils.getImage(Res.IMG_CASTLE)
  private val start = Utils.getImage(Res.START_ICON)

  private var mario: Mario = _
  private var enemies = Seq[Enemy]()
  private var objects = Seq[Object]()
  private var collectibles = Seq[Collectible]()

  private val imgFlag = Utils.getImage(Res.IMG_FLAG)
  private val imgCastle = Utils.getImage(Res.IMG_CASTLE_FINAL)


  initializeGameEntities()

  this.setFocusable(true)
  this.requestFocusInWindow

  this.addKeyListener(new Keyboard)

  def getMario: Mario = this.mario

  def getFloorOffsetY: Int = this.floorOffsetY

  def getHeightLimit: Int = this.heightLimit

  def getMov: Int = this.mov

  def getXPos: Int = this.xPos

  def setXPos(xPos: Int): Unit = this.xPos = xPos

  def setFloorOffsetY(floorOffsetY: Int): Unit = this.floorOffsetY = floorOffsetY

  def setHeightLimit(heightLimit: Int): Unit = this.heightLimit = heightLimit

  def setMov(mov: Int): Unit = this.mov = if (getMario.power) mov * 2 else mov

  def setBackground1PosX(x: Int): Unit = this.background1PosX = x

  def setBackground2PosX(x: Int): Unit = this.background2PosX = x

  def updateBackgroundOnMovement(): Unit = {
    if (this.getXPos >= 0 && this.getXPos <= 4600) {
      this.setXPos(this.getXPos + this.getMov)
      this.background1PosX = this.background1PosX - this.getMov
      this.background2PosX = this.background2PosX - this.getMov
    }
    if (this.background1PosX == -800) this.background1PosX = 800
    else if (this.background2PosX == -800) this.background2PosX = 800
    else if (this.background1PosX == 800) this.background1PosX = -800
    else if (this.background2PosX == 800) this.background2PosX = -800
  }

  def updateLives(lives:Int): Unit = {
    this.lives = lives
  }

  override def paintComponent(g: Graphics): Unit = {
    super.paintComponent(g)
    val g2 = g.asInstanceOf[Graphics2D]

    objects foreach (`object` => {
      if (this.mario isEntityNearby `object`) this.mario contact `object`
      this.enemies foreach (enemy => if (enemy isEntityNearby `object`) enemy contact `object`)
    })

    this.collectibles foreach (collectible => {
      if (this.mario contactCollectible collectible) {
        if (collectible.isInstanceOf[Piece]) Audio.playSound(Res.AUDIO_MONEY)
        this.collectibles = this.collectibles filter (_ != collectible)
      }
    })

    this.enemies foreach (firstEnemy => {
      this.enemies foreach (secondEnemy => {
        if ((firstEnemy ne secondEnemy) && (firstEnemy isEntityNearby secondEnemy) && (firstEnemy.alive && secondEnemy.alive)) firstEnemy contact secondEnemy
      })
      if ((this.getMario isEntityNearby firstEnemy) && firstEnemy.alive) this.getMario contact firstEnemy
    })

    this.updateBackgroundOnMovement()
    if (this.getXPos >= 0 && this.getXPos <= 4600) {
      this.objects foreach (`object` => `object`.moveOnSceneShift())
      this.collectibles foreach (collectible => collectible.moveOnSceneShift())
      this.enemies foreach (enemy => enemy.moveOnSceneShift())
    }
    g2.drawImage(this.imgBackground1, this.background1PosX, 0, null)
    g2.drawImage(this.imgBackground2, this.background2PosX, 0, null)
    g2.drawImage(this.castle, 10 - this.xPos, 95, null)
    g2.drawImage(this.start, 220 - this.xPos, 234, null)

    this.objects foreach (`object` => g2.drawImage(`object`.imageObject, `object`.x, `object`.y, null))
    this.collectibles foreach (collectible => g2.drawImage(collectible.imageObject, collectible.x, collectible.y, null))

    g2.drawImage(this.imgFlag, Platform.FLAG_X_POSITION - this.xPos, Platform.FLAG_Y_POSITION, null)
    g2.drawImage(this.imgCastle, Platform.CASTLE_X_POSITION - this.xPos, Platform.CASTLE_Y_POSITION, null)

    g2.drawString(Platform.LIVES_STRING + this.lives, Platform.LIVES_X_POSITION, Platform.LIVES_Y_POSITION)

    if (this.mario.jumping) g2.drawImage(this.mario.doJump(), this.mario.x, this.mario.y, null)
    else g2.drawImage(this.mario.walk(Res.MARIO), this.mario.x, this.mario.y, null)

    this.enemies foreach (enemy => {
      if (enemy.alive) g2.drawImage(enemy.walk, enemy.x, enemy.y, null)
      else g2.drawImage(enemy.deadImage, enemy.x, enemy.y + enemy.deadOffsetY, null)
    })
  }

  private def initializeGameEntities() = {
    initializeMario()
    initializeEnemies()
    initializeObjects()
  }

  private def initializeEnemies() = {
    initializeMushrooms()
    initializeTurtles()
  }

  private def initializeObjects() = {
    initializeStaticObjects()
    initializeCollectibles()
  }

  private def initializeStaticObjects() = {
    initializeTunnels()
    initializeBlocks()
  }

  private def initializeCollectibles() = {
    initializeStars()
    initializePieces()
    initializeOneUps()
  }

  private def initializeMario() = this.mario = Mario(300, 245)

  private def initializeMushrooms() = {
    this.enemies = this.enemies :+ Enemy(Res.MUSHROOM, 800, 263)
    this.enemies = this.enemies :+ Enemy(Res.MUSHROOM, 1000, 263)
    this.enemies = this.enemies :+ Enemy(Res.MUSHROOM, 1560, 263)
    this.enemies = this.enemies :+ Enemy(Res.MUSHROOM, 1750, 263)
    this.enemies = this.enemies :+ Enemy(Res.MUSHROOM, 2650, 263)
    this.enemies = this.enemies :+ Enemy(Res.MUSHROOM, 3800, 263)

  }

  private def initializeTurtles() = {
    this.enemies = this.enemies :+ Enemy(Res.TURTLE, 900, 243)
    this.enemies = this.enemies :+ Enemy(Res.TURTLE, 1200, 243)
    this.enemies = this.enemies :+ Enemy(Res.TURTLE, 2600, 243)
    this.enemies = this.enemies :+ Enemy(Res.TURTLE, 2870, 243)
    this.enemies = this.enemies :+ Enemy(Res.TURTLE, 3090, 243)
    this.enemies = this.enemies :+ Enemy(Res.TURTLE, 3870, 243)
    this.enemies = this.enemies :+ Enemy(Res.TURTLE, 4000, 243)
  }

  private def initializeTunnels() = {
    this.objects = this.objects :+ Object(Res.TUNNEL, 600, 230)
    this.objects = this.objects :+ Object(Res.TUNNEL, 1000, 230)
    this.objects = this.objects :+ Object(Res.TUNNEL, 1600, 230)
    this.objects = this.objects :+ Object(Res.TUNNEL, 1900, 230)
    this.objects = this.objects :+ Object(Res.TUNNEL, 2500, 230)
    this.objects = this.objects :+ Object(Res.TUNNEL, 3000, 230)
    this.objects = this.objects :+ Object(Res.TUNNEL, 3800, 230)
    this.objects = this.objects :+ Object(Res.TUNNEL, 4500, 230)
  }

  private def initializeBlocks() = {
    this.objects = this.objects :+ Object(Res.BLOCK, 400, 180)
    this.objects = this.objects :+ Object(Res.BLOCK, 1200, 180)
    this.objects = this.objects :+ Object(Res.BLOCK, 1270, 170)
    this.objects = this.objects :+ Object(Res.BLOCK, 1340, 160)
    this.objects = this.objects :+ Object(Res.BLOCK, 2000, 180)
    this.objects = this.objects :+ Object(Res.BLOCK, 2600, 160)
    this.objects = this.objects :+ Object(Res.BLOCK, 2650, 180)
    this.objects = this.objects :+ Object(Res.BLOCK, 3500, 160)
    this.objects = this.objects :+ Object(Res.BLOCK, 3550, 140)
    this.objects = this.objects :+ Object(Res.BLOCK, 4000, 170)
    this.objects = this.objects :+ Object(Res.BLOCK, 4200, 200)
    this.objects = this.objects :+ Object(Res.BLOCK, 4300, 210)
  }

  private def initializeStars() = this.collectibles = this.collectibles :+ Collectible(Res.STAR, 400, 140)

  private def initializePieces() = {
    this.collectibles = this.collectibles :+ Collectible(Res.PIECE, 1202, 140)
    this.collectibles = this.collectibles :+ Collectible(Res.PIECE, 1342, 40)
    this.collectibles = this.collectibles :+ Collectible(Res.PIECE, 1650, 145)
    this.collectibles = this.collectibles :+ Collectible(Res.PIECE, 2650, 145)
    this.collectibles = this.collectibles :+ Collectible(Res.PIECE, 3000, 135)
    this.collectibles = this.collectibles :+ Collectible(Res.PIECE, 3400, 125)
    this.collectibles = this.collectibles :+ Collectible(Res.PIECE, 4200, 145)
    this.collectibles = this.collectibles :+ Collectible(Res.PIECE, 4600, 40)
  }

  private def initializeOneUps() = {
    this.collectibles = this.collectibles :+ Collectible(Res.ONEUP, 1272, 140)
  }
}