package game;

import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

public class Keyboard implements KeyListener {

    @Override
    public void keyPressed(KeyEvent e) {

        if (Main.scene.getMario().alive()) {
            if (e.getKeyCode() == KeyEvent.VK_RIGHT) {

                // per non fare muovere il castello e start
                if (Main.scene.getXPos() == -1) {
                    Main.scene.setXPos(0);
                    Main.scene.setBackground1PosX(-50);
                    Main.scene.setBackground2PosX(750);
                }
                Main.scene.getMario().moving_$eq(true);
                Main.scene.getMario().toRight_$eq(true);
                Main.scene.setMov(1); // si muove verso sinistra
            } else if (e.getKeyCode() == KeyEvent.VK_LEFT) {

                if (Main.scene.getXPos() == 4601) {
                    Main.scene.setXPos(4600);
                    Main.scene.setBackground1PosX(-50);
                    Main.scene.setBackground2PosX(750);
                }

                Main.scene.getMario().moving_$eq(true);
                Main.scene.getMario().toRight_$eq(false);
                Main.scene.setMov(-1); // si muove verso destra
            }
            // salto
            if (e.getKeyCode() == KeyEvent.VK_UP) {
                Main.scene.getMario().jumping_$eq(true);
                Audio.playSound("/resources/audio/jump.wav");
            }

        }
    }

    @Override
    public void keyReleased(KeyEvent e) {
        Main.scene.getMario().moving_$eq(false);
        Main.scene.setMov(0);
    }

    @Override
    public void keyTyped(KeyEvent e) {
    }

}
